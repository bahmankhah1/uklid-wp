<?php

date_default_timezone_set( 'Asia/Tehran' );

function require_class( $classname ) {
	global $version;
	$filename = strtolower( $classname );
	if ( strhas( $filename, 'model' ) ) {
		$filename = str_replace( "model", "", $filename );
		require_once( "model/$filename.php" );
	} else if ( $filename == 'db' ) {
		require_once( "include/db.php" );
	} else {
		require_once( "$version/$filename.php" );
	}

	return;
}


function allowed_request_method( $method ) {
	$method         = strtoupper( $method );
	$current_method = $_SERVER['REQUEST_METHOD'];
	if ( $method != $current_method ) {
		die( header( "HTTP/1.0 403 access denied" ) );
	}
}

function json_result( $code, $message, $data ) {
	$error_codes   = array(
		'500',
		'403'
	);
	$success_codes = array(
		'200'
	);


	if ( in_array( $code, $success_codes ) ) {
		return $data;
	} else {
		die( header( "HTTP/1.0 $code $message" ) );
	}
}

function validate_user_token() {
	$headers = get_header_data();
	global $prefix;
	if ( ! isset( $headers['token'] ) ) {
		return false;
	}
	$user_token = sanitize_text_field( $headers['token'] );
	global $wpdb;
	$check = $wpdb->get_row( "SELECT * FROM " . $prefix . "user_tokens WHERE token LIKE '$user_token'" );
	if ( $check ) {
		return $check->user_id;
	}

	return false;
}

function get_header_data() {
	$headers = apache_request_headers();
	$headers = array_combine( array_map( 'ucwords', array_keys( $headers ) ), array_values( $headers ) );
	foreach ( $headers as $key => $value ) {
		$headers[ strtolower( $key ) ] = strtolower( $value );
	}

	return $headers;
}

function get_json_data() {
	$entityBody = file_get_contents( 'php://input' );
	$entityBody = json_decode( $entityBody );

	return $entityBody;
}

function makedir( $target ) {
	if ( ! file_exists( $target ) ) {
		$result = mkdir( $target, 0777, true );
	}

	return true;
}

// upload files
function upload_file( $file ) {
	global $config;

	$upload_dir = $_SERVER['DOCUMENT_ROOT'] . '/wp-content/uploads/profile/';
	$newname    = "profile-" . date( 'YmdHis' ) . rand( 1000, 9999 ) . ".jpg";
	$path       = date( 'Y' ) . "/" . date( 'm' ) . "/" . date( 'd' );
	$dist       = $upload_dir . $path . '/';

	$url = $config['home_url'] . '/wp-content/uploads/profile/' . $path . '/' . $newname;

	$mkdir = makedir( $dist );
	if ( $mkdir ) {
		$move = move_uploaded_file( $file['tmp_name'][0], $dist . $newname );
		$now  = date( 'Y-m-d H:i:s' );
		if ( $move ) {
			return $url;
		}
	}

	return null;
}

function get_user_ip() {
	if ( ! empty( $_SERVER['HTTP_CLIENT_IP'] ) ) {
		$ip_address = $_SERVER['HTTP_CLIENT_IP'];
	} //whether ip is from proxy
	elseif ( ! empty( $_SERVER['HTTP_X_FORWARDED_FOR'] ) ) {
		$ip_address = $_SERVER['HTTP_X_FORWARDED_FOR'];
	} //whether ip is from remote address
	else {
		$ip_address = $_SERVER['REMOTE_ADDR'];
	}

	return $ip_address;
}

function get_radnom_unique_username( $uprefix = 'user' ) {
	global $prefix;
	$user_exists = 1;
	do {
		$rnd_str = sprintf( "%0d", mt_rand( 1, 999999 ) );
		global $wpdb;
		$username    = $uprefix . $rnd_str;
		$user_exists = $wpdb->get_row( "SELECT * FROM " . $prefix . "users WHERE user_login LIKE $rnd_str" );
	} while ( $user_exists > 0 );

	return $uprefix . $rnd_str;
}

function generate_password( $length = 12, $special_chars = true, $extra_special_chars = false ) {
	$chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
	if ( $special_chars ) {
		$chars .= '!@#$%^&*()';
	}
	if ( $extra_special_chars ) {
		$chars .= '-_ []{}<>~`+=,.;:/?|';
	}

	$password = '';
	for ( $i = 0; $i < $length; $i ++ ) {
		$password .= substr( $chars, rand( 0, strlen( $chars ) - 1 ), 1 );
	}

	return $password;
}

function get_user_meta( $user_id, $meta_key = null ) {
	global $wpdb;
	global $prefix;
	// return meta for meta_key
	if ( $meta_key ) {
		$meta_key = sanitize_text_field( $meta_key );
		$usermeta = $wpdb->get_row( "SELECT * FROM " . $prefix . "usermeta WHERE `user_id` = '$user_id' AND `meta_key` = '$meta_key'" );

		return $usermeta->meta_value;
	}

	// to return all user metas
	$usermetas = $wpdb->get_results( "SELECT * FROM " . $prefix . "usermeta WHERE user_id = '$user_id'" );
	if ( $usermetas ) {
		foreach ( $usermetas as $usermeta ) {
			$output[ $usermeta->meta_key ] = $usermeta->meta_value;
		}

		return $output;
	}

	// if user doesnt has any meta
	return null;
}

function get_from_array( $array, $index ) {
	if ( ! $array || $index === null ) {
		return null;
	}

	if ( isset( $array[ $index ] ) ) {
		return $array[ $index ];
	}

	return null;
}

function replace_extra_enters( $text ) {
//	echo $text; exit;
	$text = str_replace( '\n\n\n\n\n', '\n\n', $text );
	$text = str_replace( '\n\n\n\n', '\n\n', $text );
	$text = str_replace( '\n\n\n', '\n\n', $text );

	return $text;
}

function allow_personal_uploads ( $existing_mimes=array() ) {

	// allow uploading .MOBI and .EPUB files
	$existing_mimes['epub'] = 'application/epub+zip';

	// return amended array
	return $existing_mimes;
}
add_filter('upload_mimes', 'allow_personal_uploads');