<?php

class Course{
    public function single($id, $short) {
		allowed_request_method( 'get' );
		$home = CourseModel::get($id, $short);
		api::send_result( 200, null, $home );
	}

    public function catalog(){
		allowed_request_method( 'get' );
        $home = CourseModel::getList();
		api::send_result( 200, null, $home );
	}
}