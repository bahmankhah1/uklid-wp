<?php

class Home {
    public function get() {
		allowed_request_method( 'get' );
		$home = HomeModel::getHome();
		api::send_result( 200, null, $home);
	}
}