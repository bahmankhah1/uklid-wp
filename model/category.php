<?php

class CategoryModel
{
    static public function getList()
    {
        global $wpdb;
        global $prefix;
        $mainHomeCats = [81, 133, 77];
        $catArray = [];
        foreach ($mainHomeCats as $catId) {
            $sql    = "SELECT * FROM `{$prefix}terms` WHERE `term_id` = $catId ";
            $mainCat = $wpdb->get_row($sql);
            $sql    = "SELECT name, slug, term_id, guid FROM `{$prefix}terms` t NATURAL JOIN `{$prefix}term_taxonomy` tt INNER JOIN `{$prefix}posts` p ON p.`post_title` = t.`name` WHERE `parent` = $catId ";
            $results = $wpdb->get_results($sql);
            array_push($catArray, ['name' => $mainCat->name, 'slug' => $mainCat->slug, 'term_id' => $mainCat->term_id, 'terms' => [],]);
            $terms = [];
            foreach ($results as $r) {
                array_push(
                    $terms,
                    [
                        'name' => $r->name,
                        'slug' => $r->slug,
                        'term_id' => $r->term_id,
                        'guid'=> $r->guid,
                    ],
                );
            }
            $catArray[array_key_last($catArray)]['terms'] = $terms;
        }
        return $catArray;
    }
}
