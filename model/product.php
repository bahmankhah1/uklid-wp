<?php

class ProductModel{
    public static function getList( $page_index = 1, $page_size = 10, $order = 'DESC', $short = false ){
        global $wpdb;
		global $prefix;
        global $queryParams;
		$offset = ( $page_index - 1 ) * $page_size;

        $catSql = '';
        if(isset($queryParams['cat'])){
            $catSql = "AND `term_taxonomy_id` = {$queryParams['cat']}";
        }

		$sql    = "SELECT * FROM {$prefix}posts LEFT JOIN {$prefix}term_relationships ON `ID` = `object_id` WHERE `post_type` = 'product' AND  `post_status` = 'publish' $catSql ORDER BY `post_date` $order LIMIT $offset,$page_size";
        
		$results = $wpdb->get_results( $sql );
		$list       = array();
        foreach($results as $result){
            $meta = get_meta('postmeta', $result->ID);
            if($short) {
                $list[] = array(
                    'title' => $result->post_title,
                    'imageUrl' => get_post_thumbnail_url(get_from_array($meta, '_thumbnail_id')),
                    'price' => get_from_array($meta, '_price'),
                    'regularPrice' => get_from_array($meta, '_regular_price'),
                    'salePrice' => get_from_array($meta, '_sale_price'),
                );
                continue;
            }
            $list[] = array(
                'title'=>$result->post_title,
                'imageUrl'=>get_post_thumbnail_url( get_from_array( $meta, '_thumbnail_id' ) ),
                'price'=>get_from_array( $meta, '_price' ),
                'regularPrice'=>get_from_array( $meta, '_regular_price' ),
                'salePrice'=>get_from_array( $meta, '_sale_price' ),
                'postDate'=>$result->post_date,
                'postContent'=>$result->post_content,
            );

        }

		return $list;
    }

    public static function get($id, $short = false){
        global $wpdb;
		global $prefix;
		$course = $wpdb->get_row( "SELECT * FROM " . $prefix . 'posts' . " WHERE `ID` = '$id' AND `post_status` = 'publish' AND `post_type` = 'product' " );
		if ( ! $course ) {
			return null;
		}
        $meta = get_meta( 'postmeta', $id );
		if($short){
			return array(
				'title'=>$course->post_title,
                'imageUrl'=>get_post_thumbnail_url( get_from_array( $meta, '_thumbnail_id' ) ),
                'price'=>get_from_array( $meta, '_price' ),
                'regularPrice'=>get_from_array( $meta, '_regular_price' ),
                'salePrice'=>get_from_array( $meta, '_sale_price' ),
			);
		}
		return array(
			'title'=>$course->post_title,
			'imageUrl'=>get_post_thumbnail_url( get_from_array( $meta, '_thumbnail_id' ) ),
			'price'=>get_from_array( $meta, '_price' ),
			'regularPrice'=>get_from_array( $meta, '_regular_price' ),
			'salePrice'=>get_from_array( $meta, '_sale_price' ),
			'postDate'=>$course->post_date,
			'postContent'=>$course->post_content,
		);

    }
}