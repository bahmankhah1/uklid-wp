<?php

class PostModel{
    public static function getList( $page_index = 1, $page_size = 10, $order = 'DESC', $short = false ){
        global $wpdb;
		global $prefix;
		$offset = ( $page_index - 1 ) * $page_size;
		$sql    = "SELECT * FROM {$prefix}posts WHERE `post_type` = 'post' AND  `post_status` = 'publish' ORDER BY `post_date` $order LIMIT $offset,$page_size";

		$results = $wpdb->get_results( $sql );
		$list       = array();
        foreach($results as $result){
            $meta = get_meta('postmeta', $result->ID);
            if($short) {
                $list[] = array(
                    'title' => $result->post_title,
                    'imageUrl' => get_post_thumbnail_url(get_from_array($meta, '_thumbnail_id')),
                    'postDate'=>$result->post_date,
                    'content'=>$result->post_content,
                );
                continue;
            }
            $list[] = array(
                'title'=>$result->post_title,
                'imageUrl'=>get_post_thumbnail_url( get_from_array( $meta, '_thumbnail_id' ) ),
                'postDate'=>$result->post_date,
                'content'=>$result->post_content,
            );

        }

		return $list;
    }

    
}