<?php

class CourseModel
{


	public static function getList($page_index = 1, $page_size = 10, $order = 'DESC', $short = false)
	{

		global $wpdb;
		global $prefix;
		$offset = ($page_index - 1) * $page_size;
		$sql    = "SELECT * FROM {$prefix}posts WHERE `post_type` = 'courses' AND `post_status` = 'publish' ORDER BY `post_date` $order LIMIT $offset,$page_size";

		$results = $wpdb->get_results($sql);
		$list       = array();
		foreach ($results as $result) {
			$meta = get_meta('postmeta', $result->ID);
			$productMeta = get_meta('postmeta', get_from_array($meta, '_tutor_course_product_id'));
			$courseVideoArray = maybe_unserialize(get_from_array($meta, '_video'));
			$courseVideo = $wpdb->get_row("SELECT guid from {$prefix}posts WHERE ID = ". $courseVideoArray['source_video_id'] ." ");
			
			if ($short) {
				$list[] = array(
					'title' => $result->post_title,
					'imageUrl' => get_post_thumbnail_url(get_from_array($meta, '_thumbnail_id')),
					'price' => get_from_array($productMeta, '_price'),
					'regularPrice' => get_from_array($productMeta, '_regular_price'),
					'salePrice' => get_from_array($productMeta, '_sale_price'),
					'courseVideoUrl' => $courseVideo['guid'],
				);
				continue;
			}
			$list[] = array(
				'title' => $result->post_title,
				'imageUrl' => get_post_thumbnail_url(get_from_array($meta, '_thumbnail_id')),
				'price' => get_from_array($productMeta, '_price'),
				'regularPrice' => get_from_array($productMeta, '_regular_price'),
				'salePrice' => get_from_array($productMeta, '_sale_price'),
				'postDate' => $result->post_date,
				'postContent' => $result->post_content,
				'courseVideoUrl' => $courseVideo->guid,
			);
		}

		return $list;
	}

	public static function get($id, $short = false)
	{
		global $wpdb;
		global $prefix;
		$course = $wpdb->get_row("SELECT * FROM " . $prefix . 'posts' . " WHERE `ID` = '$id' AND `post_status` = 'publish' AND `post_type` = 'courses' ");
		if (!$course) {
			return null;
		}
		$meta = get_meta('postmeta', $id);
		$result = array();
		if ($short) {
			return array(
				'title' => $course->post_title,
				'imageUrl' => get_post_thumbnail_url(get_from_array($meta, '_thumbnail_id')),
				'price' => get_from_array($meta, '_price'),
				'regularPrice' => get_from_array($meta, '_regular_price'),
				'salePrice' => get_from_array($meta, '_sale_price'),
			);
		}
		return array(
			'title' => $course->post_title,
			'imageUrl' => get_post_thumbnail_url(get_from_array($meta, '_thumbnail_id')),
			'price' => get_from_array($meta, '_price'),
			'regularPrice' => get_from_array($meta, '_regular_price'),
			'salePrice' => get_from_array($meta, '_sale_price'),
			'postDate' => $course->post_date,
			'postContent' => $course->post_content,
		);
	}
}
