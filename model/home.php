<?php

class HomeModel{
    static public function getHome(){
        $cats = CategoryModel::getList();
        return ['cats'=> $cats, 'get'=>$_GET];
    }
    static public function getHomeOld(){
        global $wpdb;
        global $prefix;
        $results = $wpdb->get_results("SELECT * FROM {$prefix}posts WHERE post_type = 'product' AND post_status = 'publish' ORDER BY post_date DESC LIMIT 5");
        $products = [];
        foreach($results as $product){
            $meta = get_meta('postmeta', $product->ID);
            
            $products[] = array(
                'title'=>$product->post_title,
                'imageUrl'=>get_post_thumbnail_url( get_from_array( $meta, '_thumbnail_id' ) ),
                'price'=>get_from_array( $meta, '_price' ),
                'regularPrice'=>get_from_array( $meta, '_regular_price' ),
                'salePrice'=>get_from_array( $meta, '_sale_price' ),
            );
        }

        $results = $wpdb->get_results("SELECT * FROM {$prefix}posts WHERE post_type = 'courses' AND post_status = 'publish' ORDER BY post_date DESC LIMIT 5");

        $courses = [];
        foreach($results as $course){
            $meta = get_meta('postmeta', $course->ID);
            
            $courses[] = array(
                'title'=>$course->post_title,
                'imageUrl'=>get_post_thumbnail_url( get_from_array( $meta, '_thumbnail_id' ) ),
                'price'=>get_from_array( $meta, '_price' ),
                'regularPrice'=>get_from_array( $meta, '_regular_price' ),
                'salePrice'=>get_from_array( $meta, '_sale_price' ),
            );
        }

        $results = $wpdb->get_results("SELECT * FROM {$prefix}posts WHERE post_type = 'post' AND post_status = 'publish' ORDER BY post_date DESC LIMIT 5");

        $posts = [];
        foreach($results as $post){
            $meta = get_meta('postmeta', $post->ID);
            
            $posts[] = array(
                'title'=>$post->post_title,
                'imageUrl'=>get_post_thumbnail_url( get_from_array( $meta, '_thumbnail_id' ) ),
                'content'=>$post->post_content,
            );
        }

        return ['products'=>$products, 'courses'=>$courses, 'posts'=>$posts];

    }
}

