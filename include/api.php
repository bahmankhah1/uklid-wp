<?php

class api {
	public $db;
	public $user_token;
	public $json;

	public function __construct() {
		global $user_token;
		global $json;
		global $prefix;
		global $headers;
		global $app_version;
		$prefix      = get_table_prefix();
		$user_token  = $this->get_user_token();
		$app_version = $this->get_app_version();
		$json        = $this->get_json_data();
		$headers     = $this->get_header_data();

//		global $wpdb;
//		$wpdb->insert( $prefix . 'options', array(
//			'option_name'  => 'test_api_' . time(),
//			'option_value' => maybe_serialize( $json )
//		) );
	}

	private function get_json_data() {
		$entityBody = file_get_contents( 'php://input' );
		$entityBody = json_decode( $entityBody );

		return $entityBody;
	}

	private function get_header_data() {
		$headers = apache_request_headers();
		$headers = array_combine( array_map( 'ucwords', array_keys( $headers ) ), array_values( $headers ) );
		foreach ( $headers as $key => $value ) {
			$headers[ strtolower( $key ) ] = strtolower( $value );
		}

		return $headers;
	}

	private function get_user_token() {
		$temp  = $this->get_header_data();
		$token = ( isset( $temp['token'] ) ) ? $temp['token'] : null;

		if ( $token ) {
			return $token;
		}

		return null;
	}

	private function get_app_version() {
		$temp        = $this->get_header_data();
		$app_version = ( isset( $temp['appversion'] ) ) ? $temp['appversion'] : null;
		if ( $app_version ) {
			return $app_version;
		}

		return null;
	}

	public static function send_result( $code, $msg, $data ) {
		$error_codes   = array(
			'500',
			'403',
			'503'
		);
		$success_codes = array(
			'200',
		);


		if ( in_array( $code, $success_codes ) ) {
			$json = json_encode( $data );

			die( replace_extra_enters( $json ) );
		} else {
			header( "HTTP/2.0 500 $code $msg" );
			echo $msg;
		}
		exit;
	}

	public static function allowed_request_method( $input ) {
		$method         = strtolower( $input );
		$current_method = strtolower( $_SERVER['REQUEST_METHOD'] );
		if ( $method !== $current_method ) {
			die( header( "HTTP/1.0 403 access denied" ) );
		}
	}

	public static function userFireWall() {
		$user = userModel::validate();
		if ( $user ) {
			return $user;
		}
		self::send_result( 403, 'forbidden', null );
	}
}
