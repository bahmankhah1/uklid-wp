<?php

date_default_timezone_set( 'Asia/Tehran' );

function strhas( $string, $search, $caseSensitive = false ) {
	if ( $caseSensitive ) {
		return strpos( $string, $search ) !== false;
	} else {
		return strpos( strtolower( $string ), strtolower( $search ) ) !== false;
	}
}

function get_request_uri() {
	return $_SERVER['REQUEST_URI'];
}


function base_url() {
	return '/bsapp';
}

function get_table_prefix() {
	global $wpdb;

	return $wpdb->prefix;
}

function uploads_dir( $path = null ) {
	global $config;
	$uploads = $config['home_url'] . '/wp-content/uploads';
	if ( ! $uploads ) {
		return $uploads;
	}

	return "$uploads/$path";
}

function get_post_thumbnail_url( $attachment_id, $size = null ) {
	global $wpdb;
	global $prefix;
	$attachment = get_meta( 'postmeta', $attachment_id );
	if ( $size ) {
		return null;
	}

	if ( ! isset( $attachment['_wp_attached_file'] ) ) {
		return null;
	}

	return uploads_dir( $attachment['_wp_attached_file'] );
}

function get_category_thumbnail_url( $attachment_id, $size = null ) {
	global $wpdb;
	global $prefix;
	$meta = get_meta( "postmeta", $attachment_id );

	return uploads_dir( $meta['_wp_attached_file'] );
}

function get_meta( $table, $id ) {
	global $wpdb;
	global $prefix;
	$columns = array(
		'postmeta'    => 'post_id',
		'usermeta'    => 'user_id',
		'commentmeta' => 'comment_id'
	);
	$datas   = $wpdb->get_results( "SELECT * FROM " . $prefix . $table . " WHERE `$columns[$table]` LIKE '$id'" );
	if ( $datas ) {
		foreach ( $datas as $item ) {
			$output[ $item->meta_key ] = $item->meta_value;
		}

		return $output;
	}

	return null;
}

function get_user( $user_id ) {
	$user_id = sanitize_text_field( $user_id );
	global $wpdb;
	global $prefix;
	$user = $wpdb->get_row( "SELECT * FROM " . $prefix . 'users' . " WHERE ID LIKE '$user_id'" );
	if ( $user ) {
		$usermeta                 = get_meta( 'usermeta', $user_id );
		$usermeta['display_name'] = $user->display_name;

		return $usermeta;
	}

	return null;
}

function get_audio_file( $id, $authorname, $content_title ) {
	global $wpdb;
	global $prefix;
	$file = $wpdb->get_row( "SELECT * FROM " . $prefix . 'posts' . " WHERE ID='$id'" );
	$meta = get_meta( 'postmeta', $id );


	if ( $file ) {
		return array(
			'title'    => $content_title,
			'subtitle' => $authorname,
			'url'      => $file->guid,
			'duration' => maybe_unserialize( $meta['_wp_attachment_metadata'] )['length_formatted']
		);
	}

	return null;
}

function get_pdf_file( $id ) {
	global $wpdb;
	global $prefix;
	$file     = $wpdb->get_row( "SELECT * FROM " . $prefix . 'posts' . " WHERE ID='$id'" );
	$filemeta = get_meta( 'postmeta', $id );
	if ( $file ) {
		return $file->guid;
	}

	return null;
}

function get_pdf_uri( $id ) {
	global $wpdb;
	global $prefix;
	$file = $wpdb->get_row( "SELECT * FROM $prefix" . "postmeta postmeta WHERE postmeta.meta_key = '_wp_attached_file' AND postmeta.post_id = $id" );
	if ( $file ) {
		return $_SERVER['DOCUMENT_ROOT'] . "/wp-content/uploads/" . $file->meta_value;
	}

	return null;
}

function get_comments_likes( $id ) {
	global $wpdb;
	global $prefix;
	$num = $wpdb->get_row( "SELECT `meta_value` as likes FROM $prefix" . "commentmeta WHERE `comment_id` = $id AND `meta_key` = '_like_count'" );

	if ( $num ) {
		return intval( $num->likes );
	}

	return 0;
}

function get_comments_dislikes( $id ) {
	global $wpdb;
	global $prefix;
	$num = $wpdb->get_row( "SELECT `meta_value` as dislikes FROM $prefix" . "commentmeta WHERE `comment_id` = $id AND `meta_key` = '_dislike_count'" );

	if ( $num ) {
		return intval( $num->dislikes );
	}

	return 0;
}

function get_post_likes( $id ) {
	global $wpdb;
	global $prefix;
	$num = $wpdb->get_row( "SELECT `meta_value` as likes FROM $prefix" . "postmeta WHERE `post_id` = $id AND `meta_key` = '_post_like_count'" );

	if ( $num ) {
		return intval( $num->likes );
	}

	return 0;
}

function get_post_dislikes( $id ) {
	global $wpdb;
	global $prefix;
	$num = $wpdb->get_row( "SELECT `meta_value` as dislikes FROM $prefix" . "postmeta WHERE `post_id` = $id AND `meta_key` = 'dislikes'" );

	if ( $num ) {
		return intval( $num->dislikes );
	}

	return 0;
}

function time_elapsed_string( $datetime, $full = false ) {
	$now  = new DateTime;
	$ago  = new DateTime( $datetime );
	$diff = $now->diff( $ago );

	$diff->w = floor( $diff->d / 7 );
	$diff->d -= $diff->w * 7;

	$string = array(
		'y' => 'سال',
		'm' => 'ماه',
		'w' => 'هفته',
		'd' => 'روز',
		'h' => 'ساعت',
		'i' => 'دقیقه',
		's' => 'ثانیه',
	);
	foreach ( $string as $k => &$v ) {
		if ( $diff->$k ) {
			$v = $diff->$k . ' ' . $v;
		} else {
			unset( $string[ $k ] );
		}
	}

	if ( ! $full ) {
		$string = array_slice( $string, 0, 1 );
	}

	return $string ? implode( ', ', $string ) . ' قبل' : 'همین الان';
}


function calculate_pages_pdf( $path ) {
	$source_path = '/home/uklidir/eurika.ir/wp-content/uploads/'.$path;
	if ( ! $fp = @fopen( $source_path, "r" ) ) {
		return null;
	}
	$max = 0;
	while ( ! feof( $fp ) ) {
		$line = fgets( $fp, 255 );
		if ( preg_match( '/\/Count [0-9]+/', $line, $matches ) ) {
			preg_match( '/[0-9]+/', $matches[0], $matches2 );
			if ( $max < $matches2[0] ) {
				$max = $matches2[0];
			}
		}
	}
	fclose( $fp );

	return intval( $max );
}


function get_pdf_size( $attachment_id ) {
	if ( ! $attachment_id ) {
		return null;
	}
	$bytes = filesize( get_pdf_uri( $attachment_id ) );

	return formatBytes( $bytes, 1 );
}

function get_content_persons( $content_id ) {
	$content_id  = intval( $content_id );
	$meta        = get_meta( 'postmeta', $content_id );
	$translators = maybe_unserialize( get_from_array( $meta, '_translators' ) );
	$speakers    = maybe_unserialize( get_from_array( $meta, '_speakers' ) );
	$persons     = [];
	if ( $translators ) {
		foreach ( $translators as $translator ) {
			$persons[] = userModel::user_object_tiny( $translator, 'مترجم' );
		}
	}

	if ( $speakers ) {
		foreach ( $speakers as $speaker ) {
			$persons[] = userModel::user_object_tiny( $speaker, 'گوینده' );
		}
	}

	return $persons;
}

function get_like_status( $content_id, $user_id ) {
	global $wpdb;
	global $prefix;

	$content_id = intval( $content_id );
	$user_id    = intval( $user_id );

	$check = $wpdb->get_row( "SELECT * FROM $prefix" . "posts_feedback WHERE `user_id` = '$user_id' AND `content_id` = '$content_id' AND `type` = 1" );
	if ( $check ) {
		return true;
	}

	return false;
}

function full_duration( $times ) {
	$minutes = 0; //declare minutes either it gives Notice: Undefined variable
	// loop throught all the times
	foreach ( $times as $time ) {
		$exploded = explode( ':', $time );
		if ( count( $exploded ) == 2 ) {
			$hour   = 0;
			$minute = $exploded[0];
		} else {
			$hour   = $exploded[0];
			$minute = $exploded[1];
		}
		$minutes += $hour * 60;
		$minutes += $minute;
	}

	$hours   = floor( $minutes / 60 );
	$minutes -= $hours * 60;
	$output  = "";
	if ( $hours ) {
		$output .= $hours . " ساعت ";
	}

	if ( $minutes ) {
		$output .= $minutes . " دقیقه ";
	}

	return $output;
}

function formatBytes( $bytes, $precision = 2 ) {
	$units = array( 'B', 'KB', 'MB', 'GB', 'TB' );

	$bytes = max( $bytes, 0 );
	$pow   = floor( ( $bytes ? log( $bytes ) : 0 ) / log( 1024 ) );
	$pow   = min( $pow, count( $units ) - 1 );

	// Uncomment one of the following alternatives
	$bytes /= pow( 1024, $pow );

	// $bytes /= (1 << (10 * $pow));

	return round( $bytes, $precision ) . ' ' . $units[ $pow ];
}